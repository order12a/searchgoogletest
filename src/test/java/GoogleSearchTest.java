import com.codeborne.selenide.ElementsCollection;
import org.junit.Assert;
import org.junit.Test;

import static com.codeborne.selenide.CollectionCondition.size;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byName;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.url;

public class GoogleSearchTest extends BaseTest{

    @Test
    public void testSearchAndFollowLink(){
        open("http://google.com/ncr");

        search("Selenium automates browsers");

        assertResultsCountEqual(10);

        followLink(0);

        $("#header>h1>a[title='Return to Selenium home page']").shouldBe(visible);
        Assert.assertEquals("http://www.seleniumhq.org/", url());
    }

    ElementsCollection searchResults = $$(".srg>.g");

    public void search(String phrase){
        $(byName("q")).val(phrase).pressEnter();
    }

    public void assertResultsCountEqual(int count){
        searchResults.shouldHave(size(count));
    }

    public void followLink(int index){
        searchResults.get(index).find("h3>a").click();
    }

}
